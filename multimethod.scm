(module multimethod

(define-multi define-method)

(import chicken scheme)

(begin-for-syntax
 (import chicken)
 (use srfi-1 matchable lolevel))

(define-for-syntax (expand-multi name dispatch #!key default (test 'eq?))
  `(define ,name
     (extend-procedure
      (lambda args
        (let* ((val (apply ,dispatch args))
               (methods (procedure-data ,name))
               (method (or (alist-ref val methods ,test)
                           (alist-ref ,default methods ,test))))
          (if method
              (apply method args)
              (error "no multi method implementation found for dispatch value" val ',name))))
      '())))

(define-syntax define-multi
  (ir-macro-transformer
   (lambda (x i c)
     (apply expand-multi (cdr x)))))


(define-syntax define-method
  (syntax-rules ()
    ((_ dispatch-val (name args ...) body ...)
     (define-method dispatch-val name (lambda (args ...) body ...)))
    ((_ dispatch-val name proc)
     (extend-procedure
      name
      (cons (cons dispatch-val proc)
            (procedure-data name))))))

)
