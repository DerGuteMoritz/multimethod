(load-relative "../multimethod")
(import multimethod)
(use test)

(define (shape-type s)
  (alist-ref 'shape s))

(define-multi area shape-type)

(define-method 'square (area s)
  (* (alist-ref 'height s) (alist-ref 'width s)))

(define-method 'circle (area s)
  (let ((radius (alist-ref 'radius s)))
    (* 3.14159265 radius radius)))

(define a-square
  '((shape . square) (width . 10) (height . 5)))

(define a-circle
  '((shape . circle) (radius . 3)))

(test 50 (area a-square))
(test 28.274333850000005 (area a-circle))

(define an-octagon '((shape . octagon) (width . 100)))
(test-error (area an-octagon))

(define-method #f (area s) #f)
(test #f (area an-octagon))
